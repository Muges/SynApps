#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module principal de SynApps

main.py
    Lance le gestionnaire de paquet

main.py update
    Lance le gestionnaire de mises à jour (affiche un message s'il n'y a
    pas de mises à jour disponibles)

main.py update silent
    Lance le gestionnaire de mises à jour (n'affiche pas de message s'il
    n'y a pas de mises à jour disponibles)

main.py install [<paquet>] (e.g. FramafoxPortable_37.0.2-fr-r02.fmk.zip)
    Lance l'installation d'un paquet

main.py install <applications> (e.g. FramafoxPortable)
    Lance l'installation d'applications depuis les dépôts
"""

import logging
from lib import constants

# Enregistrement des messages de debug dans le fichier debug.log
logging.basicConfig(level=logging.DEBUG, filename=constants.DEBUG,
                    format='%(asctime)-15s - %(levelname)-8s : %(message)s')

# Affichage des message de debug dans la console
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(logging.Formatter('%(message)s'))
logging.getLogger('').addHandler(console)

import gtk, traceback, sys, os
from lib import messagedialog

def excepthook(exception_type, value, trace):
    """
    Fonction appelée en cas d'exception non rattrapée.
    """

    # Logue la trace de l'erreur
    logging.error(''.join(traceback.format_exception(exception_type, value, trace)))

    # Affiche un message d'erreur
    messagedialog.error("Une erreur s'est produite; pour plus de détails, " +
                        "consultez le fichier %s." % (constants.DEBUG,))

    # Quitte gtk
    try:
        gtk.main_quit()
    except RuntimeError:
        pass

    # Supprime le fichier de session
    try:
        os.remove(constants.SESSION)
    except IOError:
        pass

    sys.exit(1)

sys.excepthook = excepthook

import gobject
from lib import main_window, db, databaseupdatedialog

def start(_, argv):
    """
    Lance SynApps
    """
    if len(argv) == 1:
        argv.append("")
    if len(argv) == 2:
        argv.append("")

    if argv[1] == "update":
        logging.info("Mode : Mise à jour")
        from lib.upgradedialog import UpgradeDialog
        dialog = UpgradeDialog(argv[2] == "silent")
    elif argv[1] == "install" and os.path.isfile(argv[2]):
        logging.info("Mode : Installation de paquet")
        from lib.packageinstaller import PackageInstaller
        dialog = PackageInstaller(argv[2])
        dialog.run()
    elif argv[1] == "install" and argv[2] == "":
        logging.info("Mode : Installation de paquet")
        from lib.packageinstaller import PackageInstaller
        dialog = PackageInstaller()
        dialog.run()
        dialog.run()
    elif argv[1] == "install":
        logging.info("Mode : Installation depuis la base de donnée")
        from lib.installdialog import InstallDialog
        dialog = InstallDialog(argv[2:])
        dialog.run()
    else:
        logging.info("Mode : Normal")
        main_window.main_window()

def main(argv):
    """
    Effectue des vérifications préliminaires, met à jour la base de donnée,
    puis lance SynApps.
    """

    logging.info("Lancement de SynApps")
    logging.info("Version %s", constants.VERSION)

    # Création des sous-dossiers de cache
    logging.debug("Création des sous-dossiers de cache")
    if not os.path.exists("cache/appinfo"):
        os.makedirs("cache/appinfo/")
    if not os.path.exists("cache/backup/"):
        os.makedirs("cache/backup/")
    if not os.path.exists("cache/icons/"):
        os.makedirs("cache/icons/")
    if not os.path.exists("cache/packages/"):
        os.makedirs("cache/packages/")

    # Évite de lancer plusieurs instances de SynApps
    if os.path.exists(constants.SESSION):
        logging.info("Le fichier %s existe : SynApps est déjà lancé.", constants.SESSION)
        messagedialog.error("SynApps est déjà en cours d'exécution.\nEn cas de problème, " +
                            "supprimez le fichier %s avant de redémarrer SynApps.",
                            os.path.abspath(constants.SESSION))
        sys.exit(1)

    # Crée le fichier de session
    logging.debug("Création du fichier %s", constants.SESSION)
    open(constants.SESSION, 'w').close()

    # Permet d'utiliser les threads
    gobject.threads_init()

    gtk.window_set_default_icon_list(gtk.gdk.pixbuf_new_from_file("icons/SynApps16.png"),
                                     gtk.gdk.pixbuf_new_from_file("icons/SynApps24.png"),
                                     gtk.gdk.pixbuf_new_from_file("icons/SynApps32.png"),
                                     gtk.gdk.pixbuf_new_from_file("icons/SynApps64.png"),
                                     gtk.gdk.pixbuf_new_from_file("icons/SynApps128.png"))

    # Initialise la base de donnée
    database = db.database()
    database.init()
    database.close()

    # Met à jour la base de donnée
    dialog = databaseupdatedialog.DatabaseUpdateDialog()
    dialog.connect("destroy", start, argv)
    dialog.run()

    gtk.main()

    # Supprime le fichier de session
    logging.debug("Suppression du fichier %s", constants.SESSION)
    os.remove(constants.SESSION)

if __name__ == "__main__":
    main(sys.argv)
