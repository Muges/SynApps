# SynApps 0.2.0.7
  
Gestionnaire d'applications pour la Framakey

## Note importante

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou 
le modifier au titre des clauses de la Licence Publique Générale GNU, 
telle que publiée par la Free Software Foundation ; soit la version 3 
de la Licence.

## Disclaimer

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS 
AUCUNE GARANTIE ; sans même une garantie implicite de COMMERCIABILITE 
ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la Licence 
Publique Générale GNU pour plus de détails. Vous devriez avoir reçu 
un exemplaire de la Licence Publique Générale GNU avec ce programme.
Les auteurs déclinent toutes responsabilités quant à l'utilisation 
qui pourrait en être faite.

## Dépendances

 - pygtk
 - [Requests](http://python-requests.org/)
 - [Certifi](http://certifi.io/) (SynApps fonctionne sans, mais utilise
   alors les certificats systèmes pour les connections https)
