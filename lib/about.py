#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Module gérant la fenêtre "À propos"
"""

import gtk
from lib import constants

def about(_):
    """
    Crée et ouvre la fenêtre "À propos"
    """
    dialog = gtk.AboutDialog()
    dialog.set_version(constants.VERSION)
    dialog.set_comments("Gestionnaire d'applications portables")

    with open("licence.txt", "r") as f:
        dialog.set_license(f.read())

    dialog.set_wrap_license(True)
    dialog.set_website("http://www.framakey.org/")
    dialog.set_authors(('Roromis <http://www.roromis.fr.nf>',
                        '',
                        'Programmé en python <http://www.python.org/>',
                        'en utilisant:',
                        ' - PyGTK <http://www.pygtk.org/>',
                        ' - py2exe <http://www.py2exe.org/>',
                        ' - htmltextview (Gustavo J. A. M. Carneiro)',
                        '',
                        'Les icônes indiquant l\'état des applications',
                        'proviennent du thème d\'icônes Faenza',
                        '<http://tiheum.deviantart.com/art/Faenza-Icons-173323228>',
                        '',
                        'Les icônes des catégories proviennent du',
                        'thème d\'icônes Humanity',
                        '<https://launchpad.net/humanity>',
                        '',
                        'L\'icône de SynApps a été créée à partir de',
                        'l\'icône de la logithèque d\'Ubuntu',
                        '<https://launchpad.net/software-center>'))
    dialog.set_logo(gtk.gdk.pixbuf_new_from_file('icons/SynApps128.png'))
    dialog.set_program_name("SynApps")

    dialog.run()
    dialog.destroy()
