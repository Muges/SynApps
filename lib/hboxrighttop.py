#!/usr/bin/python
# -*- coding: utf-8 -*-

import gtk
from lib import srclist
    
class hboxrighttop(gtk.HBox):
    def __init__(self):
        gtk.HBox.__init__(self)
        self.set_border_width(5)
        
        self.search_label = gtk.Label("Rechercher : ")
        self.pack_start(self.search_label, False, False)
        
        self.search_entry = gtk.Entry()
        self.pack_start(self.search_entry, True, True)
        
        self.search_button = gtk.Button()
        self.search_button.set_image(gtk.image_new_from_stock(gtk.STOCK_FIND, gtk.ICON_SIZE_MENU))
        self.pack_start(self.search_button, False, False)
        
        self.display_label = gtk.Label("    Afficher : ")
        self.pack_start(self.display_label, False, False)
        
        self.srclist = srclist.srclist()
        self.pack_start(self.srclist, False, False)
