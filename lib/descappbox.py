#!/usr/bin/python
# -*- coding: utf-8 -*-

import gtk, os.path, webbrowser, htmltextview
from lib import db

def get_str_size(size):
    unite = "o"
    if size >= 1024:
        size /= 1024.
        unite = "Ko"
        if size >= 1024:
            size /= 1024.
            unite = "Mo"
            if size >= 1024:
                size /= 1024.
                unite = "Go"
    return str(round(size, 2)) + " " + unite

class DescAppBox(gtk.EventBox):
    def __init__(self):
        self.database = db.database()
        
        gtk.EventBox.__init__(self)
        self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        self.HBox = gtk.HBox()
        
        self.vbox = gtk.VBox()
        self.add(self.vbox)
        self.vbox.set_border_width(2)
        
        self.VBoxHaut = gtk.VBox()
        
        self.HBBFramakey = gtk.HButtonBox()
        self.HBBFramakey.set_layout(gtk.BUTTONBOX_START)
        self.HBBFramasoft = gtk.HButtonBox()
        self.HBBFramasoft.set_layout(gtk.BUTTONBOX_START)
        self.HBBWebsite = gtk.HButtonBox()
        self.HBBWebsite.set_layout(gtk.BUTTONBOX_START)
        
        self.Icon = gtk.Image()
        self.Icon.set_padding(2, 2)
        self.App = gtk.Label("")
        self.App.set_padding(2, 2)
        self.App.set_alignment(0, 0.5)
        self.App.set_use_markup(True)
        self.App.set_selectable(True)
        self.Desc = gtk.Label("")
        self.Desc.set_padding(2, 2)
        self.Desc.set_line_wrap(True)
        self.Desc.set_use_markup(True)
        self.Desc.set_alignment(0, 0.5)
        self.Desc.set_selectable(True)
        
        #self.LongDesc = gtk.Label("")
        #self.LongDesc.set_padding(2, 2)
        #self.LongDesc.set_line_wrap(True)
        #self.LongDesc.set_use_markup(True)
        #self.LongDesc.set_alignment(0, 0.5)
        
        self.LongDesc = htmltextview.HtmlTextView()
        self.LongDesc.connect("url-clicked", self.url_clicked)
        
        self.FramakeyLink = gtk.LinkButton("http://www.framakey.org/", "Fiche Framakey")
        self.FramasoftLink = gtk.LinkButton("http://www.framasoft.net/", "Fiche Framasoft")
        self.Website = gtk.LinkButton("http://www.framakey.org/", "Site")
        self.Licence = gtk.Label("")
        self.Licence.set_padding(2, 2)
        self.Licence.set_alignment(0, 0.5)
        self.Licence.set_selectable(True)
        self.Size = gtk.Label("")
        self.Size.set_padding(2, 2)
        self.Size.set_alignment(0, 0.5)
        self.Size.set_selectable(True)
        self.ZipSize = gtk.Label("")
        self.ZipSize.set_padding(2, 2)
        self.ZipSize.set_alignment(0, 0.5)
        self.ZipSize.set_selectable(True)
        
        self.VBoxHaut.pack_start(self.App, False, False)
        self.VBoxHaut.pack_start(self.Desc, False, False)
        
        self.HBox.pack_start(self.Icon, False, False)
        self.HBox.pack_start(self.VBoxHaut, True, True)
        
        self.vbox.pack_start(self.HBox, False, False)
        self.vbox.pack_start(self.LongDesc, False, False)
        
        self.HBBFramakey.pack_start(self.FramakeyLink, False, False)
        self.HBBFramasoft.pack_start(self.FramasoftLink, False, False)
        self.HBBWebsite.pack_start(self.Website, False, False)
        
        self.vbox.pack_start(self.HBBFramakey, False, False)
        self.vbox.pack_start(self.HBBFramasoft, False, False)
        self.vbox.pack_start(self.HBBWebsite, False, False)
        self.vbox.pack_start(self.Licence, False, False)
        self.vbox.pack_start(self.Size, False, False)
        self.vbox.pack_start(self.ZipSize, False, False)
    
    def url_clicked(self, widget, url, type):
        webbrowser.open_new_tab(url)
    
    def display(self, infos):
        if infos['parentname'] != "" and infos['parentname'] != None:
            self.App.set_markup("<big><big><b>" + infos['parentname'] + "</b></big></big>")
            self.App.show()
        else:
            self.App.hide()
        
        if os.path.isfile(infos['icon']):
            self.Icon.set_from_file(infos['icon'])
            self.Icon.show()
        elif os.path.isfile("img/" + infos['name'] + ".png"):
                self.Icon.set_from_file("img/" + infos['name'] + ".png")
                self.Icon.show()
        else:
            self.Icon.hide()
            
        if infos['desc'] != "" and infos['desc'] != None:
            self.Desc.set_markup("<b>" + infos['desc'] + "</b>")
            self.Desc.show()
        else:
            self.Desc.hide()
        
        self.LongDesc.get_buffer().set_text("")
        if infos['longdesc'] != "" and infos['longdesc'] != None:
            self.LongDesc.display_html("<body>" + infos['longdesc'].encode("utf8") + "</body>")
            self.LongDesc.show()
        else:
            self.LongDesc.hide()
        
        if infos['framakey']!= "" and infos['framakey'] != None:
            self.FramakeyLink.set_uri(infos['framakey'])
            self.FramakeyLink.show()
        else:
            self.FramakeyLink.hide()
        

        if infos['framasoft'] != "" and infos['framasoft'] != None:
            self.FramasoftLink.set_uri(infos['framasoft'])
            self.FramasoftLink.show()
        else:
            self.FramasoftLink.hide()
        

        if infos['website'] != "" and infos['website'] != None:
            self.Website.set_uri(infos['website'])
            self.Website.show()
        else:
            self.Website.hide()
            
        if infos['licence'] != "" and infos['licence'] != None:
            self.Licence.set_markup("<b>Licence:</b> " + infos['licence'])
            self.Licence.show()
        else:
            self.Licence.hide()
            
        if infos['size'] != 0:
            self.Size.set_markup("<b>Taille décompressée:</b> " + get_str_size(infos['size']))
            self.Size.show()
        else:
            self.Size.hide()
            
        if infos['zipsize'] != 0:
            self.ZipSize.set_markup("<b>Taille de l'archive:</b> " + get_str_size(infos['zipsize']))
            self.ZipSize.show()
        else:
            self.ZipSize.hide()
    
    def display_app(self, id):
        self.display(self.database.get_app(id))

    def hide_all(self):
        self.Icon.hide()
        self.App.hide()
        self.Desc.hide()
        self.LongDesc.hide()
        self.FramakeyLink.hide()
        self.FramasoftLink.hide()
        self.Website.hide()
        self.Licence.hide()
        self.Size.hide()
        self.ZipSize.hide()
    
    def destroy(self):
        self.database.close()
        gtk.EventBox.destroy(self)
