#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Module gérant les téléchargement de fichiers en http(s)
"""

import requests
from requests.exceptions import RequestException
from lib.constants import TIMEOUT

session = requests.Session()

def download(url, path):
    """
    Télécharge un fichier.

    Args:
        url (str): adresse du fichier à télécharger
        path (str): destination du téléchargement
    """
    with open(path, "wb") as f:
        response = session.get(url, timeout=TIMEOUT)
        f.write(response.content)
        response.close()


def download_chunk(url, path):
    """
    Télécharge un fichier par morceaux.

    Args:
        url (str): adresse du fichier à télécharger
        path (str): destination du téléchargement

    Yields:
        int: taille téléchargée
        int: taille totale du fichier

    Examples:
        >>> for current_size, total_size in download_chunk(url, path):
        ...     print "%d / %d" % (current_size, total_size)
    """
    with open(path, "wb") as f:
        response = session.get(url, timeout=TIMEOUT, stream=True)
        total_size = response.headers.get('content-length')

        if total_size is None:
            f.write(response.content)
        else:
            current_size = 0
            total_size = int(total_size)
            chunk_size = max(1024*8, total_size/100)
            for data in response.iter_content(chunk_size):
                current_size += len(data)
                f.write(data)

                yield (current_size, total_size)

        response.close()

def get(url):
    """
    Télécharge le contenu d'un fichier et le renvoie sous forme de
    chaîne de caractères.

    Args:
        url (str): adresse du fichier à télécharger

    Returns:
        str: le contenu du fichier
    """
    response = session.get(url, timeout=TIMEOUT)
    content = response.content
    response.close()

    return content

def check_connection():
    """
    Vérifie que la connexion internet fonctionne

    Returns:
        bool: True si l'application a accés à internet
    """
    try:
        response = session.head("https://files.framakey.org/index.html", timeout=TIMEOUT)
        response.close()
        return True
    except RequestException:
        return False

def set_proxy(hostname, port, username, password):
    """
    Configure le proxy

    Args:
        hostname (str): Nom de domaine du proxy
        port (int): Numéro de port du proxy
        username (str): Nom d'utilisateur ("" pour un proxy sans authentification)
        password (str): Mot de passe ("" pour un proxy sans authentification)
    """
    proxy_info = {'host': hostname,
                  'port': port,
                  'user': username,
                  'pass': password}
    if username == "":
        proxy = "http://%(host)s:%(port)d" % proxy_info
    else:
        proxy = "http://%(user)s:%(pass)s@%(host)s:%(port)d" % proxy_info
        
    session.proxies = {"http": proxy,
                       "https": proxy}

def unset_proxy():
    """
    Réinitialise la configuration du proxy
    """
    session.proxies = {}
