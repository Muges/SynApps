#!/usr/bin/python
# -*- coding: utf-8 -*-

import gtk

class NoteBookImg(gtk.Notebook):
    def __init__(self):
        gtk.Notebook.__init__(self)
        
    def append_page(self, frame, label = None, image = None):
        tabBox = gtk.VBox(False, 2)
        if image is not None:
            tabImage = gtk.image_new_from_file(image)
            tabBox.pack_start(tabImage, False)
        if label is not None:
            tabLabel = gtk.Label(label)
            tabBox.pack_start(tabLabel, False)
        tabBox.show_all()
        
        gtk.Notebook.append_page(self, frame, tabBox)
