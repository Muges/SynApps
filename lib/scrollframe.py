#!/usr/bin/python
# -*- coding: utf-8 -*-

import gtk

class scrollframe(gtk.Frame):
    def __init__(self, hpolicy=gtk.POLICY_AUTOMATIC, vpolicy=gtk.POLICY_AUTOMATIC):
        gtk.Frame.__init__(self)
        
        self.scroll = gtk.ScrolledWindow()
        self.scroll.set_policy(hpolicy, vpolicy)
        
        self.add(self.scroll)

    def add_with_viewport(self, child):
        self.scroll.add_with_viewport(child)
        self.scroll.get_children()[0].set_shadow_type(gtk.SHADOW_NONE)
