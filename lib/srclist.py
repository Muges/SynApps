#!/usr/bin/python
# -*- coding: utf-8 -*-

import gtk, gobject
from lib import db

class srclist(gtk.ComboBox):
    
    __gsignals__ = {
        "toggle-src": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
    }
    
    def __init__(self):
        self.store = store()
        
        gtk.ComboBox.__init__(self, self.store)
        
        self.toggle_cellrenderer = gtk.CellRendererToggle()
        self.connect("changed", self.select)
        self.text_cellrenderer = gtk.CellRendererText()
        
        self.pack_start(self.toggle_cellrenderer, True)
        self.pack_start(self.text_cellrenderer, True)
        self.add_attribute(self.toggle_cellrenderer, 'activatable', 0)
        self.add_attribute(self.toggle_cellrenderer, 'active', 1)
        self.add_attribute(self.text_cellrenderer, 'text', 2)
        
    def select(self, widget):
        """Activé quand on clique sur un élément du menu afficher."""
        iter = self.get_active_iter()
        if iter != None:
            self.toggle(widget, self.store.get_path(iter))
            self.set_active(-1)
    
    def toggle(self, widget, path):
        self.store.toggle(path)
        
        self.emit("toggle-src")
    
    def refresh(self):
        self.store.display_sources()
    
    def destroy(self):
        self.store.close()
        gtk.ComboBox.destroy(self)

class store(gtk.ListStore):
    def __init__(self):
        self.database = db.database()
        gtk.ListStore.__init__(self, gobject.TYPE_BOOLEAN, gobject.TYPE_BOOLEAN, str, str, int)
        
        self.display_sources()
    
    def display_sources(self):
        self.clear()
        for source in self.database.get_sources():
            self.append([True, source["active"], source["name"], source["url"], source["srcid"]])
    
    def toggle(self, path):
        active = not self[path][1]
        self[path][1] = active
        self.database.set_active(self[path][4], active)
    
    def close(self):
        self.database.close()
