#!/usr/bin/python
# -*- coding: utf-8 -*-

import gtk

class hbboxbottom(gtk.HButtonBox):
    def __init__(self):
        gtk.HButtonBox.__init__(self)
        self.set_layout(gtk.BUTTONBOX_START)
        self.set_border_width(5)
        
        self.about = self.add_button(stock=gtk.STOCK_ABOUT)
        self.pref = self.add_button(stock=gtk.STOCK_PREFERENCES)
        self.refresh = self.add_button(stock=gtk.STOCK_REFRESH, secondary=True)
        self.cancel = self.add_button(stock=gtk.STOCK_CANCEL, secondary=True)
        self.apply = self.add_button(stock=gtk.STOCK_APPLY, secondary=True)
    
    def add_button(self, label=None, stock=None, use_underline=True, secondary=False):
        button = gtk.Button(label, stock, use_underline)
        self.pack_start(button, False, False)
        self.set_child_secondary(button, secondary)
        return button
