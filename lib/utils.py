# -*- coding: utf-8 -*-

"""
Module définissant des fonctions utilitaires
"""


def human_readable_size(size):
    """
    Args:
        size (int): taille en octet
    
    Returns:
        str: taille lisible (e.g. "2,35 Mo")
    """
    unit = "o"
    if size >= 1024:
        size /= 1024.
        unit = "Ko"
        if size >= 1024:
            size /= 1024.
            unit = "Mo"
            if size >= 1024:
                size /= 1024.
                unit = "Go"
    return "%.2f %s" % (size, unit)
