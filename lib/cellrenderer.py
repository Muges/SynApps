#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#      cellrenderer.py
#
#      Copyright 2010 Roromis <admin@roromis.fr.nf>
#
#      This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 2 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program; if not, write to the Free Software
#      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#      MA 02110-1301, USA.

"""
Affiche l'état d'une application dans un gtk.TreeView, et permet de le
modifier
"""

import gtk, gobject

class CellRendererAppToggle(gtk.CellRendererPixbuf):
    """
    Affiche l'état d'une application dans un gtk.TreeView, et permet de
    le modifier
    """
    __gproperties__ = {"app_state": (gobject.TYPE_INT, "app_state", "app_state",
                                     0, 2, 0, gobject.PARAM_READWRITE),
                       "app_change": (gobject.TYPE_INT, "app_change", "app_change",
                                      0, 2, 0, gobject.PARAM_READWRITE)}

    def __init__(self):
        gtk.CellRendererPixbuf.__init__(self)
        self.set_property("mode", gtk.CELL_RENDERER_MODE_ACTIVATABLE)

        gobject.signal_new("toggled", CellRendererAppToggle,
                           gobject.SIGNAL_RUN_LAST,
                           gobject.TYPE_NONE,
                           (gobject.TYPE_PYOBJECT,))

        self.icon_list = [["icons/package-uninstalled.png",        #0, 0
                           "icons/package-install.png",            #0, 1
                           "icons/package-install.png"],           #0, 2
                          ["icons/package-uninstall.png",          #1, 0
                           "icons/package-installed-outdated.png", #1, 1
                           "icons/package-upgrade.png"],           #1, 2
                          ["icons/package-uninstall.png",          #1, 0
                           "icons/package-installed-updated.png",  #1, 1
                           "icons/package-installed-updated.png"]] #1, 2

        self.set_property("pixbuf", gtk.gdk.pixbuf_new_from_file(self.icon_list[0][0]))

    def do_set_property(self, pspec, value):
        """
        Modifie une propriété de l'objet
        """
        if pspec.name == "app_state":
            if value == 0 or value == 1 or value == 2:
                setattr(self, "app_state", value)
                setattr(self, "app_change", value)
            else:
                raise ValueError("app_state property must be 0, 1 or 2")
        elif pspec.name == "app_change":
            if value == 0 or value == 1 or value == 2:
                setattr(self, "app_change", value)
            else:
                raise ValueError("app_change property must be 0, 1 or 2")
        else:
            setattr(self, pspec.name, value)

    def do_get_property(self, pspec):
        """
        Renvoie une propriété de l'objet
        """
        return getattr(self, pspec.name)

    def do_render(self, *args, **kwargs):
        """
        Affiche une icône correspondant à l'état de l'application
        """
        state = self.get_property("app_state")
        change = self.get_property("app_change")
        self.set_property("pixbuf", gtk.gdk.pixbuf_new_from_file(self.icon_list[state][change]))
        gtk.CellRendererPixbuf.do_render(self, *args, **kwargs)

    def do_activate(self, event, wid, path, bg_area, cell_area, flags):
        """
        Méthode appelée quand on clique sur l'icône
        """
        self.emit("toggled", path)
        return True

gobject.type_register(CellRendererAppToggle)
