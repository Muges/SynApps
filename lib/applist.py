#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Widget affichant la liste des applications d'une catégorie
"""

import gtk, gobject, pango, glib
from lib import cellrenderer, db
from lib.utils import human_readable_size

class AppList(gtk.TreeView):
    """
    Widget affichant la liste des applications d'une catégorie
    """

    __gsignals__ = {
        "select-app": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (int, )),
    }

    def __init__(self):
        # Id de la catégorie affichée
        self.current_id = 0

        self.store = AppStore()

        gtk.TreeView.__init__(self, self.store)
        self.set_headers_visible(False)

        toggle_cellrenderer = cellrenderer.CellRendererAppToggle()
        toggle_column = gtk.TreeViewColumn('')
        toggle_column.pack_start(toggle_cellrenderer, True)
        toggle_column.add_attribute(toggle_cellrenderer, 'app_state', 0)
        toggle_column.add_attribute(toggle_cellrenderer, 'app_change', 1)
        self.append_column(toggle_column)

        icon_cellrenderer = gtk.CellRendererPixbuf()
        icon_column = gtk.TreeViewColumn('')
        icon_column.pack_start(icon_cellrenderer, True)
        icon_column.add_attribute(icon_cellrenderer, 'pixbuf', 2)
        self.append_column(icon_column)

        name_cellrenderer = gtk.CellRendererText()
        name_cellrenderer.set_property('ellipsize', pango.ELLIPSIZE_MIDDLE)
        name_column = gtk.TreeViewColumn('Application', name_cellrenderer, markup=3)
        name_column.set_sort_order(gtk.SORT_ASCENDING)
        name_column.add_attribute(name_cellrenderer, 'text', 3)
        self.append_column(name_column)

        self.connect("cursor-changed", self._select)
        toggle_cellrenderer.connect("toggled", self.store.toggle)

    def _select(self, _):
        """
        Méthode appelée quand une application est selectionnée
        """
        (store, treeiter) = self.get_selection().get_selected()
        self.emit("select-app", store.get_value(treeiter, 4))

    def display_apps(self, cat, search=""):
        """
        Affiche les applications d'une catégorie

        Args:
            cat (int): id de la catégorie
            search (str): chaîne de caractère recherchée
        """
        self.current_id = cat
        self.refresh(search)

    def refresh(self, search=""):
        """
        Affiche les applications de la catégorie

        Args:
            search (str): chaîne de caractère recherchée
        """
        self.store.display_apps(self.current_id, search)

    def destroy(self):
        """
        Ferme la connection à la base de donnée avant de détruire le
        widget
        """
        self.store.close()
        gtk.TreeView.destroy(self)

class AppStore(gtk.ListStore):
    """
    gtk.ListStore contenant une liste d'applications
    """
    def __init__(self):
        self.database = db.database()
        gtk.ListStore.__init__(self, int, int, gtk.gdk.Pixbuf, str, int)

    def display_apps(self, cat, search=""):
        """
        Affiche les applications d'une catégorie

        Args:
            cat (int): id de la catégorie
            search (str): chaîne de caractère recherchée
        """
        self.clear()
        for app in self.database.get_apps(cat, search):
            try:
                icon = gtk.gdk.pixbuf_new_from_file(app["icon"]) \
                          .scale_simple(24, 24, gtk.gdk.INTERP_BILINEAR)
            except glib.GError:
                icon = gtk.gdk.pixbuf_new_from_file("icons/default-app.png") \
                          .scale_simple(24, 24, gtk.gdk.INTERP_BILINEAR)
            try:
                version = self.database.get_downloads(app["appid"])[0]["version"]
            except IndexError:
                version = ""
            description = "<big>%s</big> <small>%s\n%s (%s)</small>" % \
                (app["parentname"], version, app["desc"], human_readable_size(app["size"]))
            self.append([app["state"], app["change"], icon, description, app["appid"]])

    def toggle(self, _, path):
        """
        Méthode appelée quand l'état d'une application est modifié
        """
        app = self.database.get_app(self[path][4])
        change = app["change"]

        change += 1
        if change > 2:
            change = 0
        if change == 1 and app["state"] != 1:
            change = 2

        self.database.set_change(app["appid"], change)

        self[path][1] = change

    def close(self):
        """
        Ferme la connection à la base de donnée
        """
        self.database.close()
