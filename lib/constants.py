# -*- coding: utf-8 -*-

"""
Constantes utilisées par SynApps
"""

# Version de SynApps
VERSION = "0.2.0.7"

# Fichier de debug
DEBUG = "debug.log"

# Fichier de session (permet d'éviter que deux instances de SynApps soient
# lancées en même temps)
SESSION = "cache/session.lock~"

# Timeout des connections, en secondes
TIMEOUT = 5
