# -*- coding: utf-8 -*-

"""
Gestionnaire de mises à jour
"""

import gtk
import gtk.glade
import glib
from lib import modifs, db
from lib.utils import human_readable_size

class UpgradeDialog(object):
    """
    Gestionnaire de mises à jour
    """
    def __init__(self, silent=False):
        self.database = db.database()

        # Construit l'interface avec glade et connecte les signaux
        self.glade = gtk.Builder()
        self.glade.add_from_file("ui/upgrade-dialog.glade")
        self.glade.connect_signals(self)

        self.dialog = self.glade.get_object("upgrade-dialog")
        self.store = self.glade.get_object("store")
        title = self.glade.get_object("title")
        description = self.glade.get_object("description")

        # Récupère la liste des applications pour lesquelles une mise à
        # jour est disponible
        apps = self.database.get_apps(-2)
        if len(apps) == 0:
            if silent:
                self.close()
                return

            title.set_text("Aucunes mises à jour disponibles")
            description.set_text("Toutes vos applications sont déjà à jour.")
        else:
            title.set_text("Des mises à jour sont disponibles")
            description.set_text("Si vous ne souhaitez pas les installer maintenant, " +
                                 "vous pouvez les retrouver plus tard dans SynApps.")

            # Ajoute les applications à la liste
            for app in apps:
                try:
                    icon = gtk.gdk.pixbuf_new_from_file(app["icon"])
                    icon = icon.scale_simple(24, 24, gtk.gdk.INTERP_BILINEAR)
                except glib.GError:
                    icon = self.dialog.render_icon(gtk.STOCK_MISSING_IMAGE,
                                                   gtk.ICON_SIZE_LARGE_TOOLBAR)

                version = self.database.get_downloads(app["appid"])[0]["version"]
                description = "<big>%s</big> <small>%s\n%s (%s)</small>" % \
                    (app["parentname"], version, app["desc"], human_readable_size(app["size"]))

                self.store.append([True, icon, description, app["appid"]])

        self.dialog.show_all()

    def on_toggle(self, _, path):
        """
        Méthode appelée quand l'état d'une application est modifié

        Args:
            path (tuple): chemin de la ligne selectionnée
        """
        self.store[path][0] = not self.store[path][0]

    def on_response(self, _, response):
        """
        Méthode appelée quand l'utilisateur clique sur un bouton ou
        ferme la fenêtre

        Args:
            response (int): 1 si l'utilisateur a cliqué sur le bouton
                Appliquer, 0 sinon
        """
        self.dialog.destroy()

        if response == 1:
            # Vérifie qu'il y a des applications à mettre à jour, et
            # reporte les changements dans la base de donnée
            upgrades = False
            for row in self.store:
                if row[0]:
                    upgrades = True
                    self.database.set_change(row[3], 2)

            if upgrades:
                # Effectue la mise à jour
                dialog = modifs.modifs()
                dialog.run()
        
        self.close()

    def close(self):
        """
        Ferme la base de donnée et quitte SynApps
        """
        self.database.close()
        gtk.main_quit()
