#!/usr/bin/python
# -*- coding: utf-8 -*-

import gtk, gobject
from lib import about, confirm, descappbox, applist, preferences, cattree, hboxrighttop, scrollframe, hbboxbottom, databaseupdatedialog, modifs, messagedialog

class main_window(gtk.Window):
    """Fenêtre principale"""
    def __init__(self):
        """Création de la fenêtre"""
        self.filtre = ""
            
        # Création de la fenetre
        gtk.Window.__init__(self)
        self.set_title("SynApps")
        self.set_default_size(700, 500)
        self.connect("destroy", self.close)
        self.set_border_width(5)
        
        self.princ_vbox = gtk.VBox()
        self.add(self.princ_vbox)
        
        # Création de la zone du haut
        self.hbox_righttop = hboxrighttop.hboxrighttop()
        self.hbox_righttop.search_button.connect("clicked", self.search)
        self.hbox_righttop.search_entry.connect("activate", self.search)
        self.hbox_righttop.srclist.connect("toggle-src", self.refresh_display)
        self.princ_vbox.pack_start(self.hbox_righttop, False, True)
        
        self.middle_hpaned = gtk.HPaned()
        self.middle_hpaned.set_position(175)
        self.middle_hpaned.set_border_width(5)
        self.princ_vbox.pack_start(self.middle_hpaned, True, True)
        
        # Création de la liste des catégories
        frame = scrollframe.scrollframe(hpolicy=gtk.POLICY_NEVER)
        self.cattree = cattree.cattree()
        self.cattree.connect("select-cat", self.select_cat)
        frame.add_with_viewport(self.cattree)
        self.middle_hpaned.add1(frame)
        
        self.vpaned_right = gtk.VPaned()
        self.vpaned_right.set_position(250)
        self.middle_hpaned.add2(self.vpaned_right)
        
        # Création de la liste des applications
        frame = scrollframe.scrollframe(hpolicy=gtk.POLICY_NEVER)
        self.applist = applist.AppList()
        self.applist.connect("select-app", self.select_app)
        frame.add_with_viewport(self.applist)
        self.vpaned_right.add1(frame)
        
        # Création de la description de l'application
        frame = scrollframe.scrollframe()
        self.descappbox = descappbox.DescAppBox()
        frame.add_with_viewport(self.descappbox)
        self.vpaned_right.add2(frame)
        
        # Création de la zone du bas
        self.hbbox_bottom = hbboxbottom.hbboxbottom()
        
        self.hbbox_bottom.about.connect("clicked", about.about)
        self.hbbox_bottom.pref.connect("clicked", self.preferences)
        self.hbbox_bottom.refresh.connect("clicked", self.refresh)
        self.hbbox_bottom.cancel.connect("clicked", self.cancel)
        self.hbbox_bottom.apply.connect("clicked", self.apply)
        
        self.princ_vbox.pack_start(self.hbbox_bottom, False, False)
        
        self.show_all()
        self.descappbox.hide_all()
        self.cattree.expand_all()
        
        self.cattree.select((0,))
    
    def preferences(self, widget):
        dialog = preferences.preferences(self)
        response = dialog.run()
        
        if response == gtk.RESPONSE_OK:
            self.refresh()
            
        dialog.destroy()
    
    def apply(self, widget):
        """Applique les modifications."""
        dialog = confirm.confirm_dialog(self)
        response = dialog.run()
        dialog.destroy()
        
        if response == True:
            dialog = modifs.modifs(self)
            dialog.connect("destroy", self.refresh)
            dialog.run()
        
    def cancel(self, widget):
        """Quitte l'application."""
        self.destroy()
        
    def close(self, widget):
        """Quitte gtk quand l'application est fermée."""
        gtk.main_quit()
    
    def refresh_display(self, widget=None):
        self.cattree.store.display_cats()
    
    def refresh(self, widget=None):
        """Actualise les listes."""
        dialog = databaseupdatedialog.DatabaseUpdateDialog(self, True)
        dialog.connect("destroy", self.refresh_end)
        dialog.run()

    def refresh_end(self, widget):
        self.descappbox.hide_all()
        
        self.hbox_righttop.srclist.refresh()
        self.cattree.refresh()
        self.cattree.expand_all()
    
    def refresh_display(self, widget):
        self.descappbox.hide_all()
        
        self.cattree.refresh()
        self.cattree.expand_all()
    
    def search(self, widget):
        """Applique les modifications."""
        self.filtre = self.hbox_righttop.search_entry.get_text()
        self.applist.refresh(self.filtre)
        
    def select_app(self, widget, id):
        """Activé quand on selectionne une application."""
        self.descappbox.display_app(id)
        
    def select_cat(self, widget, id):
        """Activé quand on selectionne une catégorie."""
        self.applist.display_apps(id, self.filtre)
