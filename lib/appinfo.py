#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Module permettant la lecture des fichiers appinfo.ini
"""

import ConfigParser, os.path, codecs, xml.dom.minidom, xml.parsers, zipfile
from lib.BeautifulSoup import BeautifulSoup

def _get(parser, section, option, default=None):
    """
    Renvoie la valeur d'une option d'une section d'un fichier de configuration,
    ou une valeur par défaut si cette option n'existe pas.

    Args:
        parser (ConfigParser.RawConfigParser): ConfigParser instance
        section (str): nom de la section
        option (str): nom de l'option
        default (optional): valeur par défaut
    """
    try:
        return parser.get(section, option)
    except ConfigParser.NoOptionError:
        return default

def _read(fileobj, appname):
    """
    Lit le fichier appinfo.ini et renvoie les informations de l'application
    dans un dictionnaire

    Args:
        fileobj (file): le fichier appinfo.ini
        appname (str): le nom de l'application

    Returns:
        dict: dictionnaire contenant les informations de l'application ou None
            si le fichier est invalide
    """

    # Lit le fichier encodé en utf-8 (avec ou sans BOM)
    reader = codecs.getreader("utf-8-sig")

    with reader(fileobj) as f:
        appinfo = ConfigParser.ConfigParser()
        appinfo.readfp(f)

        infos = {}

        try:
            infos["name"] = _get(appinfo, "Details", "Name", appname)
            infos["parentname"] = _get(appinfo, "Framakey", "Name", appname)
            infos["desc"] = _get(appinfo, "Details", "Description", "")

            longdesc = _get(appinfo, "Framakey", "LongDesc", "").replace("& ", "&amp; ")
            infos["longdesc"] = BeautifulSoup(longdesc).prettify()
            try:
                xml.dom.minidom.parseString("<p>" + infos["longdesc"] + "</p>")
            except xml.parsers.expat.ExpatError:
                infos["longdesc"] = ""

            size = _get(appinfo, "Framakey", "Size", 0)
            try:
                infos["size"] = int(size)
            except ValueError:
                infos["size"] = 0
            infos["zipsize"] = 0

            infos["exe"] = _get(appinfo, "Control", "Start", appname + ".exe")
            infos["dir"] = _get(appinfo, "Framakey", "Dir", appname)
            infos["installdir"] = _get(appinfo, "Framakey", "Installdir", "")

            dirsave = _get(appinfo, "Framakey", "Dirsave", "").split("|")
            infos["dirsave"] = [dirname for dirname in dirsave if dirname != ""]
            infos["dirsave"].append("Data")

            infos["version"] = _get(appinfo, "Version", "PackageVersion", 0)

            infos["author"] = _get(appinfo, "Details", "Publisher", "")
            infos["licence"] = _get(appinfo, "Framakey", "License", "")

            infos["framakey"] = _get(appinfo, "Details", "HomePage", "")
            infos["website"] = _get(appinfo, "Framakey", "AppWebsite", "")
            infos["framasoft"] = _get(appinfo, "Framakey", "FramasoftPage", "")

            infos["category"] = _get(appinfo, "Details", "Category", "Autres")
            infos["type"] = _get(appinfo, "Framakey", "Type", "apps")
            if infos["type"] == "webapps":
                infos["type"] = "WebApps"
            elif infos["type"] == "apps":
                infos["type"] = "Applications"
        except ConfigParser.NoSectionError:
            raise IOError("Le fichier appinfo.ini est invalide.")

    return infos

def read_app(path):
    """
    Lit le fichier appinfo.ini d'une application installée et renvoie les
    informations de l'application dans un dictionnaire

    Args:
        path (str): chemin du dossier contenant l'application

    Returns:
        dict: dictionnaire contenant les informations de l'application ou None
            si le dossier ne contient pas d'application valide
    """
    appname = os.path.basename(path)

    appinfo = os.path.join(path, "App/AppInfo/appinfo.ini")
    with open(appinfo, "r") as fileobj:
        infos = _read(fileobj, appname)

    appicon = os.path.join(path, "App/AppInfo/appicon_64.png")
    if os.path.isfile(appicon):
        infos["icon"] = appicon
    else:
        infos["icon"] = "icons/autres.png"

    return infos

def read_package(path):
    """
    Lit le fichier appinfo.ini d'un paquet et renvoie les informations de
    l'application dans un dictionnaire

    Args:
        path (str): chemin du paquet

    Returns:
        dict: dictionnaire contenant les informations de l'application
    """
    try:
        package = zipfile.ZipFile(path, 'r')
    except:
        raise IOError(path + " n'est pas une archive zip.")

    filename = os.path.basename(path)
    appname = filename.rsplit("_", 1)[0]

    try:
        with package.open(appname + "/App/AppInfo/appinfo.ini", "r") as fileobj:
            infos = _read(fileobj, appname)
    except KeyError:
        raise IOError(path + " ne contient pas de fichier appinfo.ini.")

    try:
        with open("cache/packages/" + appname + ".png", "wb") as f:
            f.write(package.read(appname + "/App/AppInfo/appicon_64.png"))
        infos["icon"] = "cache/packages/" + appname + ".png"
    except (KeyError, IOError):
        infos["icon"] = "icons/install.png"

    return infos
