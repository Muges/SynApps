#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module définissant des fonctions auxilliaires permettant d'afficher des
boîtes de dialogue simples.
"""

import gtk

def message_dialog(parent, message, message_type):
    """
    Affiche une boîte de dialogue contenant un message et une icône
    représentant le type de message.

    Args:
        parent (gtk.Window): Fenêtre parente (None par défaut)
        message (str): Message de la boîte de dialogue
        message_type (int): Type de message (gtk.MESSAGE_INFO,
            gtk.MESSAGE_WARNING, gtk.MESSAGE_QUESTION ou gtk.MESSAGE_ERROR)
    """
    dialog = gtk.MessageDialog(parent, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                               message_type, gtk.BUTTONS_CLOSE, message)
    dialog.run()
    dialog.destroy()

def error(message, parent=None):
    """
    Affiche une boîte de dialogue contenant un message d'erreur

    Args:
        message (str): Message de la boîte de dialogue
        parent (gtk.Window): Fenêtre parente (None par défaut)
    """
    message_dialog(parent, message, gtk.MESSAGE_ERROR)

def info(message, parent=None):
    """
    Affiche une boîte de dialogue contenant un message d'information

    Args:
        message (str): Message de la boîte de dialogue
        parent (gtk.Window): Fenêtre parente (None par défaut)
    """
    message_dialog(parent, message, gtk.MESSAGE_INFO)

def warning(message, parent=None):
    """
    Affiche une boîte de dialogue contenant un message d'avertissement

    Args:
        message (str): Message de la boîte de dialogue
        parent (gtk.Window): Fenêtre parente (None par défaut)
    """
    message_dialog(parent, message, gtk.MESSAGE_WARNING)
