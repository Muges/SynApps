# -*- coding: utf-8 -*-

"""
Fenêtre affichant la progression de la mise à jour de la base de donnée
"""

import gtk, gobject, threading, sys
from lib import db, messagedialog

class DatabaseUpdateDialog(gtk.Dialog):
    """
    Fenêtre affichant la progression de la mise à jour de la base de donnée
    """
    def __init__(self, parent=None, force=False):
        """
        Constructeur
        
        Args:
            parent (gtk.Window): Fenêtre parente (None par défaut)
            force (bool): Force la mise à jour de la base de donnée,
                même si les dépôts n'ont pas été modifiés
        """
        self.force = force
        
        # Thread effectuant la mise à jour de la base de donnée
        self.thread = None
        
        # Si il y a une erreur pendant la mise à jour, contient le
        # message d'erreur. None sinon
        self.error = None
        
        # Création de la fenêtre
        gtk.Dialog.__init__(self, "Mise à jour des dépôts", parent,
                            gtk.DIALOG_MODAL|gtk.DIALOG_DESTROY_WITH_PARENT|gtk.DIALOG_NO_SEPARATOR)
        self.set_border_width(5)
        
        label = gtk.Label()
        label.set_use_markup(True)
        label.set_markup("<b>Mise à jour des dépôts</b>")
        self.get_content_area().pack_start(label, False, False)
    
        label = gtk.Label("Téléchargement des bases de données d'applications")
        self.get_content_area().pack_start(label, False, False)
        
        self.progress = gtk.ProgressBar()
        self.progress.set_text("Mise à jour en cours...")
        self.progress.set_pulse_step(0.02)
        self.get_content_area().pack_start(self.progress, False, False)
    
    def _pulse(self):
        """
        Méthode appelée à intervalle régulier pendant la mise à jour 
        """
        if self.thread.isAlive():
            # Modifie la barre de progression
            self.progress.pulse()
            return True
        else:
            if self.error != None:
                # Il y a eut une erreur pendant la mise à jour
                messagedialog.error(self.error, self)
            
            self.destroy()
            return False
    
    def run(self):
        """
        Ouvre la fenêtre et lance la mise à jour de la base de donnée
        """
        self.show_all()
        
        self.thread = DatabaseUpdateThread(self, self.force)
        self.thread.start()
        gobject.timeout_add(50, self._pulse)

class DatabaseUpdateThread(threading.Thread):
    """
    Thread effectuant la mise à jour de la base de donnée
    """
    def __init__ (self, parent, force):
        """
        Constructeur
        
        Args:
            parent (DatabaseUpdateDialog): Fenêtre affichant la progression
            force (bool): Force la mise à jour de la base de donnée,
                même si les dépôts n'ont pas été modifiés
        """
        threading.Thread.__init__(self)
        self.parent = parent
        self.force = force
 
    def run(self):
        """
        Effectue la mise à jour
        """
        try:
            database = db.database()
            database.update(self.force)
            database.close()
        except IOError as message:
            # En cas d'erreur modifie l'attribut error de la fenêtre
            # pour qu'elle affiche le message d'erreur (on ne peut pas
            # ouvrir de boîte de dialogue dans un thread)
            self.parent.error = str(message)
